package com.IR.formgenerator.service;

import com.IR.formgenerator.domain.FormInformation;
import com.IR.formgenerator.domain.FormRecord;
import com.IR.formgenerator.entity.ProcessedForm;
import com.IR.formgenerator.enums.Status;
import com.IR.formgenerator.repository.ProcessedFormRepository;
import com.IR.formgenerator.responses.FindAllFormsResponse;
import com.IR.formgenerator.utility.CSVUtility;
import com.IR.formgenerator.utility.PDFUtility;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.lowagie.text.DocumentException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {
  Logger log = LoggerFactory.getLogger(FileService.class);
  private final AmazonS3 amazonS3;
  final RabbitTemplate rabbitTemplate;
  final ProcessedFormRepository processedFormRepository;
  @Value("${s3.bucket.name}")
  private String s3BucketName;
  @Value("${s3.bucket.folder}")
  private String folder;
  @Value("${s3.url}")
  private String s3BucketURL;

  public FileService(AmazonS3 amazonS3, RabbitTemplate rabbitTemplate, ProcessedFormRepository processedFormRepository) {
    this.amazonS3 = amazonS3;
    this.rabbitTemplate = rabbitTemplate;
    this.processedFormRepository = processedFormRepository;
  }

  @Async
  public S3ObjectInputStream findByName(String fileName) {
    log.info("Downloading file with name {}", fileName);
    return amazonS3.getObject(s3BucketName.concat("/").concat(folder), fileName).getObjectContent();
  }

  @Async
  public void processFile(MultipartFile file, String initiator) throws DocumentException, IOException {
    log.info("Processing file : {}.", file.getOriginalFilename());

    ProcessedForm formToProcess = new ProcessedForm();
    formToProcess.setTimestampProcessed(new Timestamp(System.currentTimeMillis()));
    formToProcess.setCSVFileInput(file.getBytes());
    formToProcess.setCsvFileName(file.getOriginalFilename());
    formToProcess.setInitiator(initiator);

    try {
      List<FormRecord> formRecords = CSVUtility.csvToFormRecord(file.getInputStream());
      log.info(formRecords.toString());

      PDFUtility thymeleaf2Pdf = new PDFUtility();
      String html = thymeleaf2Pdf.parseThymeleafTemplate(formRecords, initiator, formToProcess.getTimestampProcessed().toString());
      thymeleaf2Pdf.generatePdfFromHtml(html, FilenameUtils.removeExtension(file.getOriginalFilename())); // saves file locally

      try {
        final File generatedFile = new File(System.getProperty("user.home") + File.separator + FilenameUtils.removeExtension(file.getOriginalFilename()).concat(".pdf"));
        final String fileName = FilenameUtils.removeExtension(file.getOriginalFilename()).concat("_" + LocalDateTime.now()).concat(".pdf");
        formToProcess.setPDFFileOutput(FileUtils.readFileToByteArray(generatedFile));
        log.info("Uploading file with name {}", fileName);

        final PutObjectRequest putObjectRequest = new PutObjectRequest(s3BucketName.concat("/").concat(folder), fileName, generatedFile);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        amazonS3.putObject(putObjectRequest);
        stopWatch.stop();
        log.info("Uploaded file stored at {}, in {} seconds.", s3BucketURL.concat(fileName), stopWatch.getTotalTimeSeconds());

        Files.delete(generatedFile.toPath());

        formToProcess.setStatus(Status.PERSISTED_TO_REMOTE);
        formToProcess.setFileDestination(s3BucketURL.concat(fileName));
        formToProcess.setPdfFileName(fileName);

        rabbitTemplate.convertAndSend("storeFileInformationQ", formToProcess);
      } catch (AmazonServiceException e) {
        log.error("Error {} occurred while uploading file", e.getLocalizedMessage());
      } catch (IOException ex) {
        log.error("Error {} occurred while deleting temporary file", ex.getLocalizedMessage());
      } catch (Exception ex) {
        log.error("Error {} occurred while processing file", ex.getLocalizedMessage());
      }

    } catch (IOException e) {
      throw new RuntimeException("fail to store csv data: " + e.getMessage());
    }
  }

  public FindAllFormsResponse findAllForms() {

    List<ProcessedForm> processedForms = processedFormRepository.findAll();
    List<FormInformation> formInformation = new ArrayList<>();

    for (ProcessedForm form: processedForms) {
      FormInformation formInfo = new FormInformation();
      formInfo.setInitiator(form.getInitiator());
      formInfo.setCsvFileName(form.getCsvFileName());
      formInfo.setPdfFileName(form.getPdfFileName());
      formInfo.setStatus(form.getStatus());

      formInformation.add(formInfo);
    }

    return new FindAllFormsResponse(formInformation, new ArrayList());
  }

  @RabbitListener(queues = "storeFileInformationQ")
  public void persistFinalFileInformation(ProcessedForm in) throws IOException {
    log.info("Inside storeFileInformationQ listener, persisting file information.");
    processedFormRepository.saveAndFlush(in);
  }
}
