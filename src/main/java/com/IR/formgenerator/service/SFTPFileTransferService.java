package com.IR.formgenerator.service;

import com.IR.formgenerator.repository.ProcessedFormRepository;
import com.IR.formgenerator.responses.TriggerFileProcessingResponse;
import com.lowagie.text.DocumentException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

@Service
public class SFTPFileTransferService {
  Logger log = LoggerFactory.getLogger(SFTPFileTransferService.class);
  @Value("${sftp.host}")
  private String host;

  @Value("${sftp.remote-directory}")
  private String remoteDirectory;

  @Value("${sftp.port}")
  private Integer port;

  @Value("${sftp.username}")
  private String username;

  @Value("${sftp.password}")
  private String password;
  private FTPClient ftp;

  final FileService fileService;
  final ProcessedFormRepository processedFormRepository;

  public SFTPFileTransferService(FileService fileService, ProcessedFormRepository processedFormRepository) {
    this.fileService = fileService;
    this.processedFormRepository = processedFormRepository;
  }

  void open() throws IOException {
    ftp = new FTPClient();
    ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
    ftp.connect(host, port);
    int reply = ftp.getReplyCode();
    if (!FTPReply.isPositiveCompletion(reply)) {
      ftp.disconnect();
      throw new IOException("Exception connecting to FTP Server");
    }

    ftp.login(username, password);
  }

  void close() throws IOException {
    ftp.disconnect();
  }

  ArrayList<String> listFiles(String path) throws IOException {
    FTPFile[] dirListing = ftp.listFiles(path);
    ArrayList<String> csvFiles = new ArrayList<>();
    for (FTPFile file : dirListing) {
      String details = file.getName();
      if (file.getName().endsWith(".csv")) {
        csvFiles.add(details);
      }
    }
    return csvFiles;
  }

  void downloadFile(String source, String destination) throws IOException {
    FileOutputStream out = new FileOutputStream(new File(destination));
    ftp.retrieveFile(source, out);
    out.close();
  }

  public ResponseEntity<TriggerFileProcessingResponse> processFiles(String initiator) throws IOException, DocumentException {
    open();

    TriggerFileProcessingResponse response = new TriggerFileProcessingResponse();

    ArrayList<String> csvFiles = listFiles(remoteDirectory);
    ArrayList<String> alreadyProcessedFiles = processedFormRepository.findAllCSVFilenames();
    csvFiles.removeAll(alreadyProcessedFiles);

    response.setToBeProcessed(csvFiles);
    response.setAlreadyProcessed(processedFormRepository.findAllCSVFilenames());

    for (String file : csvFiles) {
      log.info("Trying to download {}", file);
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      downloadFile(remoteDirectory.concat("/").concat(file), System.getProperty("user.home") + File.separator + file);
      stopWatch.stop();

      log.info("Downloaded file from FTP at {}, in {} seconds.", remoteDirectory.concat("/").concat(file), stopWatch.getTotalTimeSeconds());

      File retrievedFile = new File(System.getProperty("user.home").concat("/").concat(file));
      FileInputStream input = new FileInputStream(retrievedFile);
      MultipartFile multipartFile = new MockMultipartFile(file,
          retrievedFile.getName(), "text/csv", IOUtils.toByteArray(input));
      input.close();

      fileService.processFile(multipartFile, initiator);
      Files.delete(Paths.get(System.getProperty("user.home") + File.separator + file));
    }
    close();
    return ResponseEntity.ok(response);
  }

   public ArrayList<String> getOnlyUnprocessedFiles() throws IOException {
    open();
    ArrayList<String> csvFiles = listFiles(remoteDirectory);
    ArrayList<String> alreadyProcessedFiles = processedFormRepository.findAllCSVFilenames();
    csvFiles.removeAll(alreadyProcessedFiles);
    close();
    return csvFiles;
  }
}
