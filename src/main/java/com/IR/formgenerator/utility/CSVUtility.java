package com.IR.formgenerator.utility;

import com.IR.formgenerator.domain.FormRecord;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class CSVUtility {
    public static List csvToFormRecord(InputStream is) throws IOException {
      return new CsvToBeanBuilder(new InputStreamReader(is))
          .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_QUOTES)
          .withIgnoreEmptyLine(true)
          .withType(FormRecord.class)
          .build()
          .parse();
    }
}
