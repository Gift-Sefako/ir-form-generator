package com.IR.formgenerator.utility;

import com.IR.formgenerator.domain.FormRecord;
import com.lowagie.text.DocumentException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class PDFUtility {
  public String parseThymeleafTemplate(List<FormRecord> records, String initiator, String timestamp) {
    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
    templateResolver.setSuffix(".html");
    templateResolver.setTemplateMode(TemplateMode.HTML);

    TemplateEngine templateEngine = new TemplateEngine();
    templateEngine.setTemplateResolver(templateResolver);

    Context context = new Context();
    context.setVariable("initiator", initiator);
    context.setVariable("fileRecords", records);
    context.setVariable("timestamp", timestamp);

    return templateEngine.process("file_records_template.html", context);
  }
  public void generatePdfFromHtml(String html, String fileName) throws DocumentException, IOException {
    String outputFolder = System.getProperty("user.home") + File.separator + fileName.concat(".pdf");
    OutputStream outputStream = Files.newOutputStream(Paths.get(outputFolder));

    ITextRenderer renderer = new ITextRenderer();
    renderer.setDocumentFromString(html);
    renderer.layout();
    renderer.createPDF(outputStream);

    outputStream.close();
  }
}
