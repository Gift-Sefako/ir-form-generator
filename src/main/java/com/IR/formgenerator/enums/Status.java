package com.IR.formgenerator.enums;

public enum Status {
  READ_FROM_REMOTE,
  PARSED_CSV,
  GENERATED_PDF,
  PERSISTED_TO_REMOTE,
  FAILED_TO_PARSE_CSV,
  FAILED_TO_CREATE_PDF,
}
