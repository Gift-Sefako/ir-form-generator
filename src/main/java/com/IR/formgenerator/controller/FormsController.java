package com.IR.formgenerator.controller;

import com.IR.formgenerator.responses.FindAllFormsResponse;
import com.IR.formgenerator.responses.TriggerFileProcessingResponse;
import com.IR.formgenerator.responses.UploadFileResponse;
import com.IR.formgenerator.service.FileService;
import com.IR.formgenerator.service.SFTPFileTransferService;
import com.lowagie.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("forms")
@CrossOrigin // For local dev purposes
public class FormsController {
  Logger log = LoggerFactory.getLogger(FormsController.class);
  final
  FileService fileService;
  final
  SFTPFileTransferService sftpFileTransferService;

  public FormsController(FileService fileService, SFTPFileTransferService sftpFileTransferService) {
    this.fileService = fileService;
    this.sftpFileTransferService = sftpFileTransferService;
  }

  @GetMapping(value = "/processed-forms", produces = "application/JSON")
  public ResponseEntity<FindAllFormsResponse> getAllProcessedForms() throws IOException {
    log.info("Retrieving all forms.");

    FindAllFormsResponse allFormsResponse = fileService.findAllForms();
    allFormsResponse.setUnprocessedForms(sftpFileTransferService.getOnlyUnprocessedFiles());
    return ResponseEntity.ok(allFormsResponse);
  }

  @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/JSON", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<UploadFileResponse> uploadFile(@RequestPart("file") MultipartFile file, @RequestParam("initiator") String initiator) {
    try {
      fileService.processFile(file, initiator);
      return ResponseEntity.status(HttpStatus.OK)
          .body(new UploadFileResponse("Received file successfully to process: " + file.getOriginalFilename()));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
          .body(new UploadFileResponse("Could not process file: " + file.getOriginalFilename() + "!"));
    }
  }

  @GetMapping(value = "download-file")
  public ResponseEntity<Object> downloadFile(@RequestParam(value = "filename") String filename) {
    return ResponseEntity
        .ok()
        .cacheControl(CacheControl.noCache())
        .header("Content-type", "application/octet-stream")
        .header("Content-disposition", "attachment; filename=\"" + filename + "\"")
        .body(new InputStreamResource(fileService.findByName(filename)));
  }

  @PostMapping(value = "trigger-processing")
  public ResponseEntity<TriggerFileProcessingResponse> triggerFileProcessing(@RequestParam("initiator") String initiator) throws IOException, DocumentException {
    log.info("Retrieving from SFTP initiated by {}.", initiator);
    return sftpFileTransferService.processFiles(initiator);
  }
}
