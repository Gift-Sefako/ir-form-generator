package com.IR.formgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class FormGeneratorForIrApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormGeneratorForIrApplication.class, args);
	}

}
