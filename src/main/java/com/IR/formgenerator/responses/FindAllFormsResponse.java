package com.IR.formgenerator.responses;

import com.IR.formgenerator.domain.FormInformation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class FindAllFormsResponse {
  List<FormInformation> processedForms;
  List<String> unprocessedForms;
}
