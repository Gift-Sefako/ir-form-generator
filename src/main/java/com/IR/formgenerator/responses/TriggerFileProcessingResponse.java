package com.IR.formgenerator.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TriggerFileProcessingResponse {
  List<String> toBeProcessed;
  List<String> alreadyProcessed;
}
