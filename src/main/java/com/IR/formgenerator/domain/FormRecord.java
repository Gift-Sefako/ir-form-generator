package com.IR.formgenerator.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class FormRecord {
  @CsvBindByName(column = "Client Name")
  private String clientName;

  @CsvBindByName(column = "Company Name")
  private String companyName;

  @CsvBindByName(column = "Number of Active Bank accounts")
  private Integer activeAccounts;

  @CsvBindByName(column = "Account Beneficiary")
  private String accountBeneficiary;
}
