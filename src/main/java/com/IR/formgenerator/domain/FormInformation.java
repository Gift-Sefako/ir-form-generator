package com.IR.formgenerator.domain;

import com.IR.formgenerator.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FormInformation {
  private String initiator;
  private String csvFileName;
  private String pdfFileName;
  private Status status;
}
