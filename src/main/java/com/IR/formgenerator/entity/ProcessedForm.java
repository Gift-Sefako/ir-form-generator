package com.IR.formgenerator.entity;

import com.IR.formgenerator.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class ProcessedForm implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long fileId;
  private String csvFileName;
  private String pdfFileName;
  @Lob
  private byte[] CSVFileInput;
  @Lob
  private byte[] PDFFileOutput;
  private Timestamp timestampProcessed;
  private String initiator;
  private String fileDestination;
  @Enumerated(EnumType.STRING)
  private Status status;

}
