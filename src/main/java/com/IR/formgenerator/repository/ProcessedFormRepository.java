package com.IR.formgenerator.repository;

import com.IR.formgenerator.entity.ProcessedForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
@Repository
public interface ProcessedFormRepository extends JpaRepository<ProcessedForm, Long> {
  @Query(value = "select csv_file_name from processed_form", nativeQuery = true)
  ArrayList<String> findAllCSVFilenames();
}
