package com.IR.formgenerator.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {
  @Bean
  public Queue storeFileInformationQ() {
    return new Queue("storeFileInformationQ", false);
  }
}
