package com.IR.formgenerator.utility;

import com.IR.formgenerator.domain.FormRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CSVUtilityTest {

  @BeforeEach
  void setUp() {
  }

  @Test
  public void givenCSVFile_with_1_record_shouldParse_1_record_correctly() throws IOException {
    String resourceName = "1_record.csv";
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(resourceName).getFile());
    String absolutePath = file.getAbsolutePath();

    assertTrue(absolutePath.endsWith(File.separator + "1_record.csv"));

    InputStream inputStream = Files.newInputStream(file.toPath());
    List<FormRecord> records = CSVUtility.csvToFormRecord(inputStream);
    assertEquals(1, records.size());

    System.out.println(absolutePath);


  }

  @Test
  public void givenCSVFile_with_0_records_shouldReturnEmptyList() throws IOException {
    String resourceName = "0_record.csv";
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(resourceName).getFile());
    String absolutePath = file.getAbsolutePath();

    assertTrue(absolutePath.endsWith(File.separator + "0_record.csv"));

    InputStream inputStream = Files.newInputStream(file.toPath());
    List<FormRecord> records = CSVUtility.csvToFormRecord(inputStream);
    assertEquals(0, records.size());

  }

  @Test
  public void givenCSVFile_withMissingDataColumns_shouldParseCorrectly() throws IOException {
    String resourceName = "missing_column_data.csv";
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(resourceName).getFile());
    String absolutePath = file.getAbsolutePath();

    assertTrue(absolutePath.endsWith(File.separator + "missing_column_data.csv"));

    InputStream inputStream = Files.newInputStream(file.toPath());
    List<FormRecord> records = CSVUtility.csvToFormRecord(inputStream);
    assertEquals(2, records.size());
    assertEquals("", records.get(1).getCompanyName());
  }

  @Test()
  public void givenCSVFile_withBadData_shouldThrowException() throws IOException {

    String resourceName = "bad_data.csv";
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(resourceName).getFile());
    String absolutePath = file.getAbsolutePath();

    assertTrue(absolutePath.endsWith(File.separator + "bad_data.csv"));

    InputStream inputStream = Files.newInputStream(file.toPath());

    Throwable exception = assertThrows(RuntimeException.class, () -> CSVUtility.csvToFormRecord(inputStream));
    assertEquals("Error parsing CSV line: 3. [Gift, ,5.6, Gift Obviously]", exception.getMessage());
  }
}
